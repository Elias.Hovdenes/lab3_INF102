package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    

    @Override
    public long sum(List<Long> list) {

        if (list.size() == 0){
            return 0;
        }
        if (list.size() == 1){
            return list.get(0);
        } else {
            Long number = list.get(list.size()-1) + list.get(list.size()-2);

            list.set(list.size()-2, number);

            list.remove(list.size()-1);

            return sum(list);
        }

    }
    

}
