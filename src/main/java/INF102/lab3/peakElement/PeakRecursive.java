package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if (numbers.size()==2){
            if (numbers.get(numbers.size()-1)>=numbers.get(numbers.size()-2)){
                return numbers.get(numbers.size()-1);
            }else{
                return numbers.get(numbers.size()-2);
                
            }
        }
        if ( numbers.get(numbers.size()-1)>=numbers.get(numbers.size()-2)){
            return numbers.get(numbers.size()-1);
        } else{
            numbers.remove(numbers.size()-1);
            return peakElement(numbers);
        }
        
    }

}
